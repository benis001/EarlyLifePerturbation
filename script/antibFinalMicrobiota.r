############# Microbiota data ####################
# This script reads in relative contribution data of microbiota, filters it and performs regression analysis using masigpro
#Read in relative contribution data and filter out the groups with low contribution at the level 3 (genera)
#Process data with proper row and column names
allMicrob <- read.delim(file="/home/user/allInfoDir/workspace/microbiota/withTmt.txt", as.is=T)
colNames <- colnames(allMicrob)
colNames <- sub("(\\.[1234567890]+)(T[1234567890]+)", "\\2\\1", colNames, perl=T)
colnames(allMicrob) <- colNames
# Rearrange the columns
dLevels <- c("J8", "J55", "J176")
dT <- c("T1", "T2", "T3")
allMicrob <- allMicrob[,unlist(lapply(dT, grep, colnames(allMicrob)))]
allMicrob <- allMicrob[,unlist(lapply(dLevels, grep, colnames(allMicrob)))]
write.table(palmicrobT, file="D:/workspace/microbiota/microbProper.txt")
palmicrobT <- read.table(file="/home/user/allInfoDir/workspace/microbiota/microbProper.txt", as.is=T)
#check the values for variance
allVar <- list()
variance <- NULL
palmicrob <- allMicrob
condition <- colnames(allMicrob)
condition <- sub("\\.[1234567890]+", "", condition)
condition <- unique(condition)
exLeast1 <- list()
for(i in 1:length(condition)) {
  nCols <- grep(condition[i], colnames(allMicrob))
  for(j in 1:dim(allMicrob)[1])
    variance[j] <- as.numeric(var(t(allMicrob[,nCols][j,])))
  allVar[[i]] <- variance
}
lapply(allVar, function(x) max(x))
#shorten microb names
shorten <- scan(file="D:/workspace/microbiota/MgroupsShort.txt", what="", sep="\n")
shorten <- sub("et rel.", "", shorten)
rownames(allMicrob) <- shorten
#check cutoff n filter
# highContri <- rowSums(almicrob > cutoff)>=4
source("/home/user/allInfoDir/workspace/newFunctions/filterPerCondition.r")
specificCols <- filterPerCondition(allMicrob, 0.1, condition) 
filteredMicrob <- allMicrob[specificCols,]
passFilt <- rownames(filteredMicrob)
write(passFilt, file="D:/workspace/microbiota/passFilt.txt")
passFilt <- scan(file="D:/workspace/microbiota/passFilt.txt", what="", sep="\t")
######## Masigpro
library(maSigPro)
#remove the three samples from animals rejected in gene expression quality control
mpoolData <- colnames(filteredMicrob)
remTmp <- c ("J8T2.8", "J176T2.8", "J176T3.4")
rnames <- mpoolData[!mpoolData %in% remTmp]
filteredMicrob <- subset(filteredMicrob, select= -c(J8T2.8, J176T2.8, J176T3.4)) #NA value in output check
expDesign <- read.table(file="/home/user/allInfoDir/workspace/testdir_ssb/masigpro/edesignJ.txt")
rownames(expDesign) <- colnames(filteredMicrob)
firstStep <- p.vector(data=filteredMicrob, design=expDesign)
# write(rownames(firstStep$SELEC), file=paste0(wd,"/microbiota/masigMicrob1st46.txt"), sep="\t")
selectedMicrob <- firstStep$SELEC
write.table(selectedMicrob, file="/home/user/allInfoDir/workspace/microbiota/selectedMasigpro.txt", sep = "\t", quote = F)
########################################
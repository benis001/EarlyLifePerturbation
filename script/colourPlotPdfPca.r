colourPlotPdfPca <- function(pca, cnames.expr.mat, day, treatment){ 
  pca <- pca
  sum.pca <- summary(pca)
  cnames.expr.mat <- cnames.expr.mat
  day <- day
  treatment <- treatment
  colors <- cnames.expr.mat
  names(colors) <- cnames.expr.mat
  colors[grep(paste0("_", day[1], "_"), colors)] <- "red"
  colors[grep(paste0("_", day[2], "_"), colors)] <- "green"
  colors[grep(paste0("_", day[3], "_"), colors)] <- "blue"
  treatmentsAll <- cnames.expr.mat
  names(treatmentsAll) <- cnames.expr.mat
  treatmentsAll[grep("_T1", treatmentsAll)] <- 1
  treatmentsAll[grep("_T2", treatmentsAll)] <- 2
  treatmentsAll[grep("_T3", treatmentsAll)] <- 3
  
  #Put the plots in a .pdf
  pdf("normal_pca.pdf", height=10, width=10) 
    plot(pca$x , main="PCA", pch=17, col=colors)
    text(pca$x, pos=1, treatmentsAll)
    mtext(paste("first two components explain ",format(100 * sum.pca$importance["Cumulative Proportion",2],digits=2),"% of variance", sep=""))
  dev.off()

}
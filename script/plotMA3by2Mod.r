plotMA3by2Mod <- function(normal) { 
  x <- class(normal)
  if(x[[1]] == "EList"){ 
    MA <- normal
    MA$E <- as.matrix(MA$E)
    narrays <- ncol(MA$E)
    npages <- ceiling(narrays/6)
    main <- colnames(MA)
    width <- 6.5
    height <- 10
    xlim <- c(3, 20)
    ylim <- c(-15, 15)
    prefix <- 'MA for Elist'
    ext <- "pdf"
    xlab <- "A"
    ylab <- "M"
    status <- MA$genes$Status
    for (ipage in 1:npages) {
      i1 <- ipage * 6 - 5
      i2 <- min(ipage * 6, narrays)
      pname = paste(prefix, "-", i1, "-", i2, ".", ext, sep = "")
      pdf(pname , height = height, width= width)
      par(mfrow = c(3, 2))
      for (i in i1:i2) {
        plotMA(MA, array = i, xlim = xlim, ylim = ylim, legend = (i%%6 == 1), zero.weights = TRUE, main = main[i])
      }
      dev.off()
    }
  
  } else { print("This function was modified only for Elist objects")}
 
}
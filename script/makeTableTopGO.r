makeTableTopGO <- function(significantGO, annotatedGenes, fname){ 
  significantGO <- significantGO
  annotatedGenes <- annotatedGenes
  nRow <- length(significantGO)
  enrichTable <- as.data.frame(matrix(rep(0, 6*nRow), nrow=nRow))
  colNames=c("RowNum", "GO.ID", "GO.Term","p.value", "GeneInList", "GeneInGO")
  colnames(enrichTable) <- colNames                             
  for(j in 1:nRow) {
    genesTerm <- annotatedGenes[[significantGO[j]]]
    tGenes <- intersect(selectEntrezIds, genesTerm)
    enrichTable$RowNum[j] <-j
    enrichTable$GO.ID[j] <-significantGO[j]
    enrichTable$GO.Term[j] <- GOTERM[[significantGO[j]]]@Term
    enrichTable$p.value[j] <- unname(pvalFis[j])
    enrichTable$GeneInList[j] <- length(tGenes)
    enrichTable$GeneInGO[j] <- length(genesTerm)
  }
  write.table(enrichTable, file=paste0(fname, ".txt"), row.names=F, quote=F, sep="\t")
}
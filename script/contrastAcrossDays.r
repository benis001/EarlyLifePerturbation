contrastAcrossDays <- function(tissue, day, treatment, f) { 
  tissue <- tissue
  day <- day
  treatment <- treatment
  f <- f
  req.contrast <- NULL
  contrast.names.tissue <- NULL
  contrast.names <- NULL
  for (i in 1:2) {
    req.tissue <- levels(f)[grep (tissue[i], levels(f))]
    for (j in 1:3){
      req.contrast <- NULL
      req.treatment <- req.tissue[grep (treatment[j], req.tissue)]
      for (k in 1:3){
        req.contrast <- append(req.contrast, req.treatment[grep (day[k], req.treatment)])
        if (k == 3)
          contrast.names.tissue[[j]] <- c(paste(req.contrast[k-2],"-", req.contrast[k-1], sep= ""), paste(req.contrast[k-1],"-", req.contrast[k], sep= ""))
      }
    }
    contrast.names[[i]] <- contrast.names.tissue
    names(contrast.names[[i]]) <- treatment
  }
  names(contrast.names) <- tissue
  return(contrast.names)
}
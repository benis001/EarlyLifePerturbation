filterPerCondition <- function(dataDF, threshold, condition){ 
  dataDF <- dataDF #a dataframe 
  threshold <- threshold # numeric
  condition <- condition # should be found in the colnames of dataDF
  cutoff <- matrix (threshold, nrow(dataDF), ncol(dataDF), byrow = TRUE)
  contribLeast1 <- list()
  for(i in 1:length(condition)) {
    nCols <- grep(condition[i], colnames(dataDF))
    highContrib <- rowSums(dataDF[,nCols] > cutoff[,nCols]) == length(nCols) 
    perCondition <- rownames(dataDF)[highContrib]
    contribLeast1[[i]] <- perCondition
    cat(condition[i], "\n", length(perCondition), "\n") #jus for debugging n compilin info
  }
  specificCols <- unique(unlist(contribLeast1))
  return(specificCols)
}
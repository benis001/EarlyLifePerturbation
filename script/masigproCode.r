library(maSigPro)
load("/home/user/allInfoDir/workspace/testdir_ssb/output/GnPnspC.RData") #data that has been normalised, low intensity is removed and gene names substituted,  where ever possible so has probe names too
jData <- reduGnPnspC$E[,grep("jejunum", colnames(reduGnPnspC$E))]
source("/home/user/allInfoDir/workspace/newFunctions/makeDesignMasigpro.r")
expDesign <- makeDesignMasigpro(targets, colnames(reduGnPnspC))
#standardise the data 
library(Mfuzz)
jDataEset <- new("ExpressionSet", expr=jData) 
featurenames <- rownames(jData)
featureNames(jDataEset)<-featurenames
SjData <- standardise(jDataEset)
# perform analysis with the wrapper function
analysisWhole <- maSigPro(exprs(SjData), Q=0.05, vars="groups", expDesign, pdf=F, cluster.method="mfuzz")

## need to make a design matrix 
makeDesignMasigpro <- function(targets, cnames.reduGnPnspC){ 
  targets <- targets #very specific to my data
  cnames.reduGnPnscp <- cnames.reduGnPnspC #sample names of the data
  dLevels <- c("jejunum_8", "jejunum_55", "jejunum_176") #timepoints
  dT <- c("T1", "T2", "T3") #treatments
  targets <- targets[unlist(lapply(dT, grep, targets$Condition)),]
  targets <- targets[unlist(lapply(dLevels, grep, targets$Condition)),]
  jnoPool <- unique(targets$Condition)[grep("jejunum", unique(targets$Condition))] # names of samples without replicates
  jpNames <-  cnames.reduGnPnspC[grep("jejunum", cnames.reduGnPnspC)] # names of samples with replicate info
  
  #generate the columns necessary for the matrix
  names(jnoPool) <- c(1:9)
  
  repl <- NULL #replicates information
  tpt <- NULL # timepoints
  tmt1 <- rep(0, 33) # treatments, I had 33 samples
  tmt2 <- rep(0, 33)
  tmt3 <- rep(0, 33)
  
  for(i in 1:length(jnoPool)) { 
    fornames <- as.character(unlist(strsplit(jnoPool[i], split = "_|-")))
    pos <- grep(jnoPool[i], jpNames)
    repl <- append(repl,as.integer(rep(names(jnoPool[i]), each=length(pos))))
    tpt <- append(tpt, as.integer(rep(fornames[2], each=length(pos))))
    if(fornames[3] == "T1")
      tmt1[pos] <- as.integer(rep(1, each=length(pos)))
    if(fornames[3] == "T2")  
      tmt2[pos] <- as.integer(rep(1, each=length(pos)))
    if(fornames[3] == "T3")
      tmt3[pos] <- as.integer(rep(1, each=length(pos)))
  }
  
  jexpDesign <- as.matrix(cbind(tpt, repl, tmt1, tmt2, tmt3))
  rownames(jexpDesign) <- jpNames
  return(jexpDesign)
}
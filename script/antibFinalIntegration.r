########## Data Integration ###############
# This script performs data preprocessing and data integration of host gene expression and microbiota
# Load libraries
library(RColorBrewer)
library(igraph)
library(mixOmics)
# load and process microbiota data
inputMicrob <- read.delim(file="/home/user/allInfoDir/workspace/microbiota/selectedMasigpro.txt", as.is=T)
inputMicrob <- t(inputMicrob)
# separate data based on conditions
tmt1Ntmt2Microb4mixomics <- inputMicrob
onlytmt1Microb4mixomics <- inputMicrob[grep("T3", rownames(inputMicrob), invert = T),]
onlytmt2Microb4mixomics <- inputMicrob[grep("T2", rownames(inputMicrob), invert = T),]
# load and process gene expression data
load("/home/user/allInfoDir/workspace/testdir_ssb/output/GnspC.RData")
inputExpression <- reduGn$E[,grep("jejunum", colnames(reduGn$E))]
inputExpression <- t(as.matrix(inputExpression))
tmt1Ntmt2 <- scan(file= paste0("/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jdue2tmt.txt"), what="")
onlyTmt1 <- scan(file= paste0("/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jonlyt2.txt"), what="")
onlyTmt2 <- scan(file= paste0("/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jonlyt3.txt"), what="")
# separate the different groups of genes for the analysis, genes
tmt1Ntmt2Exprsn <- inputExpression[,colnames(inputExpression) %in% tmt1Ntmt2]
onlyTmt1Exprsn <- inputExpression[,colnames(inputExpression) %in% onlyTmt1]
onlyTmt2Exprsn <- inputExpression[,colnames(inputExpression) %in% onlyTmt2]
# separate the different groups of genes for the analysis, conditions
tmt1Ntmt2Exprsn4mixomics <- tmt1Ntmt2Exprsn
onlyTmt1Exprsn4mixomics <- onlyTmt1Exprsn[grep("T3", rownames(onlyTmt1Exprsn), invert = T),]
onlyTmt2Exprsn4mixomics <- onlyTmt2Exprsn[grep("T2", rownames(onlyTmt2Exprsn), invert = T),]
## mixomics anlaysis
# calculate similarities
ncomp = 8 #number of components is usually number of conditions - 1
tmt1Ntmt2Spars <- spls(X = tmt1Ntmt2Microb4mixomics, Y = tmt1Ntmt2Exprsn4mixomics, ncomp=ncomp)
ncomp = 5
onlyTmt1Spars <- spls(X = onlytmt1Microb4mixomics, Y = onlyTmt1Exprsn4mixomics, ncomp=ncomp)
onlyTmt2Spars <- spls(X = onlytmt2Microb4mixomics, Y = onlyTmt2Exprsn4mixomics, ncomp=ncomp)
# build networks
color.edge <- colorRampPalette(c("darkgreen", "green", "yellow", "red", "darkred")) # make a color palette for the network
ncomp = 8
netResultTmt1Ntmt2 <- network(tmt1Ntmt2Spars, comp = 1:ncomp, keep.var = TRUE, shape.node = c("rectangle", "rectangle"), color.node = c("white", "pink"), color.edge = color.edge(10), alpha = 3, threshold = 0.8)
ncomp = 5
netResultOnlyTmt1 <- network(onlyTmt1Spars, comp = 1:ncomp, keep.var = TRUE, shape.node = c("rectangle", "rectangle"), color.node = c("white", "pink"), color.edge = color.edge(10), alpha = 3, threshold = 0.8)
netResultOnlyTmt2 <- network(onlyTmt2Spars, comp = 1:ncomp, keep.var = TRUE, shape.node = c("rectangle", "rectangle"), color.node = c("white", "pink"), color.edge = color.edge(10), alpha = 3, threshold = 0.8)

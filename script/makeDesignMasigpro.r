makeDesignMasigpro <- function(targets, cnames.reduGnPnspC){ 
  targets <- targets
  cnames.reduGnPnscp <- cnames.reduGnPnspC
  dLevels <- c("jejunum_8", "jejunum_55", "jejunum_176")
  dT <- c("T1", "T2", "T3")
  targets <- targets[unlist(lapply(dT, grep, targets$Condition)),]
  targets <- targets[unlist(lapply(dLevels, grep, targets$Condition)),]
  jnoPool <- unique(targets$Condition)[grep("jejunum", unique(targets$Condition))]
  jpNames <-  cnames.reduGnPnspC[grep("jejunum", cnames.reduGnPnspC)]
  
  #generate the columns necessary for the matrix
  names(jnoPool) <- c(1:9)
  
  repl <- NULL
  tpt <- NULL
  tmt1 <- rep(0, 33)
  tmt2 <- rep(0, 33)
  tmt3 <- rep(0, 33)
    
  for(i in 1:length(jnoPool)) { 
    fornames <- as.character(unlist(strsplit(jnoPool[i], split = "_|-")))
    pos <- grep(jnoPool[i], jpNames)
    repl <- append(repl,as.integer(rep(names(jnoPool[i]), each=length(pos))))
    tpt <- append(tpt, as.integer(rep(fornames[2], each=length(pos))))
    if(fornames[3] == "T1")
      tmt1[pos] <- as.integer(rep(1, each=length(pos)))
    if(fornames[3] == "T2")  
      tmt2[pos] <- as.integer(rep(1, each=length(pos)))
    if(fornames[3] == "T3")
      tmt3[pos] <- as.integer(rep(1, each=length(pos)))
  }
  
  jexpDesign <- as.matrix(cbind(tpt, repl, tmt1, tmt2, tmt3))
  rownames(jexpDesign) <- jpNames
  return(jexpDesign)
}
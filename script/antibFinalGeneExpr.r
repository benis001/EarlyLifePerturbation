# Script to load Agilent data, visualise with limma and analyse using maSigPro

##load libraries
library(Biobase)
library(limma)
library(VennDiagram)
# Input for the gene expression microarray data
##read targets with the experimental design, map sample files to conditions
targets <- readTargets("/home/user/allInfoDir/workspace/testdir_ssb/targets_1.txt") #without replicate information
poolData <- readTargets("/home/user/allInfoDir/workspace/testdir_ssb/targets_pool.txt") #with replicate information
##read in raw data as an ElistRaw object (limma)
rawData <- read.maimages(targets,green.only = TRUE, source = "agilent.median", path = "/home/user/allInfoDir/workspace/testdir_ssb/ileumNjejunum") #from LIMMA for Agilent microarrays
write.table (rawData, file = "/testdir_ssb/raw_data.txt", sep = "\t")

###Quality Control
#colors for controls
spotTypes <- readSpotTypes("/home/user/allInfoDir/workspace/testdir_ssb/spotTypesAllGrouped.txt") #information on controls in the microarray for quality control
spotTypes$SpotType
rawData$genes$Status <- controlStatus(spotTypes, rawData$genes)
##background correction,use normalize mexp method and add offset 1 (in case we want to take logs) and normalize (quantile normalization method)
rawBgcorrect <- backgroundCorrect(rawData, method = "normexp", offset = 1)
normal <- normalizeBetweenArrays(rawBgcorrect, method = "quantile") #makes it an Elist object
normalExprsnData <- normal$E #the expression part of the Elist object
#changes the filenames to condition names just to make the graphs readable
colnames(normalExprsnData) <- poolData$Condition
##MA plots for whole normal$E adapted from the limma function plotMA3by2()
#gives .pdf files with 6 plots in each
source("/home/user/allInfoDir/workspace/newFunctions/plotMA3by2Mod.r")
plotMA3by2Mod(normal)

##Fitting, ranking and merging biological replicates
# substitute the probe names with gene names where ever possible, but retain the unmapped probes
mapProbeGene <- read.delim("PnGn.txt", as.is = T)
#use the information from agilent to map probe names to gene names
normalNoControl <- normal[normal$genes$ControlType == 0,]
replacedProbeName <- normalNoControl$genes$ProbeName
position <- which(mapProbeGene$ProbeName %in% normalNoControl$genes$ProbeName)
replacedProbeName <- replace(replacedProbeName, position, mapProbeGene$GeneName)
normWithGeneName <- normalNoControl
normWithGeneName$genes$ProbeName <- replacedProbeName
rownames(normWithGeneName$E) <- replacedProbeName
save(normWithGeneName, file="D:/workspace/testdir_ssb/output/NormalGnPn.RData")
## average over probes that map to the same gene
normGeneProbeAvg <- avereps(normWithGeneName, ID=normWithGeneName$gene$ProbeName)
colnames(normGeneProbeAvg) <- poolData$Condition

## Remove some probes (lower percentile and duplicate probes)
#Get the highest of the dark spots to get a base value and then multiply by 1.1
negative95 <- apply (normal$E [normal$gene$ControlType == -1,], 2, function(x) quantile(x, p = 0.95))
cutoff <- matrix (1.1 * negative95, nrow(normGeneProbeAvg), ncol(normGeneProbeAvg), byrow = TRUE)
#A spot is "expressed" if it is above the threshold in at least half of the conditions 
isExpressed <- rowSums(normGeneProbeAvg$E > cutoff) >= dim(targets)[1]/2
condition <- unique(targets$Condition)
exprsdInAtleast1 <- list()
for(i in 1:length(condition)) {
  nCols <- grep(condition[i], colnames(normGeneProbeAvg$E))
  isExpressed <- rowSums(normGeneProbeAvg$E[,nCols] > cutoff[,nCols])== length(nCols) 
  perCondition <- normGeneProbeAvg$genes$ProbeName[isExpressed]
  exprsdInAtleast1[[i]] <- perCondition
}
specificCols <- unique(unlist(exprsdInAtleast1))
reduGeneProbeSpC <- normGeneProbeAvg[specificCols,]
save(reduGeneProbeSpC, file= "/home/user/allInfoDir/workspace/testdir_ssb/output/reduGeneProbeSpC.RData")

## PCA plots
#Limit columns, change according to requirements
#For expressed genes
#ExprGenes <- avgGenes[match(i.t3.days,avgGenes$genes$ProbeName),]
#For all the genes
exprGenes <- normal[match(i.t3.days,normal$genes$ProbeName),]
tissue <- "jejunum"
onlyJejunum <- avgGenes[,grep("jejunum", colnames(avgGenes))]
expr.mat <- as.matrix(onlyJejunum$E)
rownames(expr.mat) <- onlyJejunum$gene$ProbeName
#Performing PCA
pca <- prcomp(t(expr.mat))
source("/home/user/allInfoDir/workspace/newFunctions/colourPlotPdfPca.r")
colourPlotPdfPca(pca, colnames(expr.mat), day, treatment)

##Hierarchical
d <- dist(t(expr.mat), method = "euclidean")
d <- as.dist(1 - cor(expr.mat))
hr <- hclust(d, method = "complete", members = NULL)
plot(hr)
dhr <- as.dendrogram(hr)
pdf("Hierarchical_pool.pdf", height = 14, width = 14)
  par( mar= c(3,10,1,10))
  plot(dhr,type = c("rectangle"), center = FALSE, edge.root = FALSE, leaflab = c("perpendicular"), dLeaf = NULL,  horiz = TRUE)
dev.off()

################## Quadratic regression analysis ##############
library(maSigPro)
load("/home/user/allInfoDir/workspace/testdir_ssb/output/GnPnspC.RData") #data that has been normalised, low intensity is removed and gene names substituted,  where ever possible so has probe names too
jData <- reduGnPnspC$E[,grep("jejunum", colnames(reduGnPnspC$E))]
source("/home/user/allInfoDir/workspace/newFunctions/makeDesignMasigpro.r")
expDesign <- makeDesignMasigpro(targets, colnames(reduGnPnspC))
#standardise the data 
library(Mfuzz)
jDataEset <- new("ExpressionSet", expr=jData) 
featurenames <- rownames(jData)
featureNames(jDataEset)<-featurenames
SjData <- standardise(jDataEset)
# perform analysis with the wrapper function
analysisWhole <- maSigPro(exprs(SjData), Q=0.05, vars="groups", expDesign, pdf=F, cluster.method="mfuzz")
#original file
load("/home/user/allInfoDir/workspace/testdir_ssb/output/maSigProJStd.RData")
analysis <- analysisWhole

listCtrl <- rownames(analysis$sig.genes$tmt1$sig.profiles)
listTmt1 <- rownames(analysis$sig.genes$tmt2vstmt1$sig.profiles)
listTmt2 <- rownames(analysis$sig.genes$tmt3vstmt1$sig.profiles)

genesTmt1 <- listTmt1[grep("A_72_",listTmt1, invert=T)]
genesTmt2 <- listTmt2[grep("A_72_",listTmt2, invert=T)]
genesTmt1 <- gsub("[[:punct:]]", "\\.",genesTmt1)
genesTmt2 <- gsub("[[:punct:]]", "\\.",genesTmt2)

tmt1Ntmt2 <- intersect(genesTmt1, genesTmt2)
onlyTmt1 <- setdiff(genesTmt1, genesTmt2)
onlyTmt2 <- setdiff(genesTmt2, genesTmt1)

tmt1Ntmt2 <- scan(file= "/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jdue2tmt.txt", sep="\t", what="")
onlyTmt1 <- scan(file= "/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jonlyt2.txt", sep="\t", what="")
onlyTmt2 <- scan(file= "/home/user/allInfoDir/workspace/testdir_ssb/masigpro/Jalone/jonlyt3.txt", sep="\t", what="")

########## TopGO script ###########
library(topGO)
# goids from biomart in convOtherDB
# humanMart <- useMart("ensembl", dataset = "hsapiens_gene_ensembl")
# goids = getBM(attributes=c('entrezgene','go_id'), filters='entrezgene', values=allEnt, mart=humanMart)
# geneID2GO <- data.frame()
# for(i in 1:length(allEnt)){ 
#   tmp <- goids[goids$entrezgene %in% allEnt[i],]
#   idTmp <- paste(tmp$go_id[grep("GO", tmp$go_id)], collapse=", ")
#   oneRow <- data.frame(entrezgene=allEnt[i], go_id=idTmp, stringsAsFactors=F)
#   if(oneRow$go_id != "")
#   geneID2GO <- rbind(geneID2GO,oneRow)
#   
# }
# 
# write.table(geneID2GO, file="geneID2GOprop.txt", sep="\t", row.names=F, col.names=F, quote=F)
geneID2GO <- readMappings(file="geneID2GOprop.txt")
propGnId <- read.table(file=paste0("/home/user/allInfoDir/workspace/conversion/propGenesIds.txt"), sep="\t", as.is=T)
allEntrez <- as.character(propGnId$Id)
selectGenesList <- list(tmt1Ntmt2, onlyTmt1, onlyTmt2)
fnameVector <- c("tmt1Ntmt2topGOBP", "onlyTmt1topGOBP", "onlyTmt2topGOBP")
for(i in 1:length(selGenesList)) { 
  fname=fnameVector[i]
  selectGenes <- selectGenesList[[i]]
  selectEntrezIds <- as.character(unique(propGnId$Id[propGnId$Gn %in% selectGenes]))
  geneList <- factor(as.integer(allEntrez %in% selectEntrezIds))
  names(geneList) <- allEntrez
  GOdataBP <- new("topGOdata", ontology = "BP", allGenes = geneList, annot = annFUN.gene2GO, gene2GO = geneID2GO)
#   save(GOdataBP, file=paste0("D:/workspace/topGO/GOdata", fname, ".RData"))
  GOdata <- GOdataBP
  annotatedGenes <- genesInTerm(GOdata) #terms for genes, opp to geneID2GO
  goNames <- names(annotatedGenes)
  test.stat <- new("classicCount", testStatistic = GOFisherTest, name = "Fisher test")
  resultFisher <- getSigGroups(GOdata, test.stat)
  pvalFis <- score(resultFisher)
  pvalFis <- sort(pvalFis)
  pval.01 <- sum(pvalFis < 0.01)
  significantGO <- names(pvalFis[1:pval.01])   
  source("/home/user/allInfoDir/workspace/newFunctions/makeTableTopGO.r")
  makeTableTopGO(significantGO, annotatedGenes, fname)
}
  